import {calculateBonuses} from "./bonus-system.js";

describe('Bonus system', () => {
    const acceptablePrecision = 5; // 5 floating point digits
    
    const standardProgram = 'Standard';
    const premiumProgram = 'Premium';
    const diamondProgram = 'Diamond';
    const nonExistentProgram = 'FooBar';

    it.each([
        [standardProgram, 500, 0.05],
        [standardProgram, 10000, 0.075],
        [standardProgram, 15000, 0.075],
        [standardProgram, 50000, 0.1],
        [standardProgram, 55000, 0.1],
        [standardProgram, 100000, 0.125],
        [standardProgram, 150000, 0.125],

        [premiumProgram, 500, 0.1],
        [premiumProgram, 10000, 0.15],
        [premiumProgram, 15000, 0.15],
        [premiumProgram, 50000, 0.2],
        [premiumProgram, 55000, 0.2],
        [premiumProgram, 100000, 0.25],
        [premiumProgram, 150000, 0.25],

        [diamondProgram, 500, 0.2],
        [diamondProgram, 10000, 0.3],
        [diamondProgram, 15000, 0.3],
        [diamondProgram, 50000, 0.4],
        [diamondProgram, 55000, 0.4],
        [diamondProgram, 100000, 0.5],
        [diamondProgram, 150000, 0.5],

        [nonExistentProgram, 500, 0],
        [nonExistentProgram, 10000, 0],
        [nonExistentProgram, 15000, 0],
        [nonExistentProgram, 50000, 0],
        [nonExistentProgram, 55000, 0],
        [nonExistentProgram, 100000, 0],
        [nonExistentProgram, 150000, 0],
    ])('For %p program, %p amount results in %p bonuses', (program, amount, expectedBonuses) => {
        const bonuses = calculateBonuses(program, amount);

        expect(bonuses).toBeCloseTo(expectedBonuses, acceptablePrecision);
    });

})